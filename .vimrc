colorscheme pablo           " Farbschema (unter $VIMRUNTIME7colors)
syntax enable               " highlight syntax

set pastetoggle=<f5>        " toggle paste mode
set encoding=utf-8          " display all chars (incl. cyrillic)
set nocompatible            " i don't care about (old-style) vi
set ai                      " Auto indent
set si                      " Smart indet
set background=dark         " Farbauswahl für Shell mit dunklem Hintergrund
set backspace=2             " solve backspace problem under cygwin
set backup                  " Backup Dateien anlegen
set backupdir=~/vim_backups " Backups in diesen Ordner speichern
set backupext=vbk           " Endung der Backup-Dateien
set confirm                 " nach Bestätigung fragen statt Speichern verweigern
set expandtab               " expand tabs to spaces
set fileformats=unix,dos    " Dateiformat autom. erkennen
set gdefault                " Standard-Flag 'g' beim ':substitute' Befehl
set hid                     " Change buffer - without saving
set hlsearch                " highlight search term
set history=999             " record a lot more vim commands than the default 20
set incsearch               " incrementelle Suche 
set loadplugins             " Plugins beim Start laden
set magic                   " Set magic on, for regular expressions
set more                    " Seitenweises Betrachten, nicht durchrollen
set shiftwidth=2            " Schrittweite bei autom. Einrückung
set showcmd                 " Kommandos nicht 'blind' eintippen:
set tabstop=2               " Schrittweite Tabstopps 
set textwidth=0             " 0 = kein Umbruch bei langen Zeilen
set whichwrap=b,s           " whichwrap -- Verhalten bei Zeilenende -- Backspace und Leertaste
set wrapscan                " Suche geht vom Dateiende zum -Anfang über
set wildmode=longest:full   " not the annoying default file name completion; do it bash like!
set wildmenu                " activate bash like file name completion (see wildmode)
"set dictionary 'dict-file' " Verzeichnisse für Schlüsselwortvervollständigung

" make folder automatically if it don't exist.
if !isdirectory(expand(&backupdir))
  call mkdir (expand(&backupdir), "p")
endif


" tab / shift width 2 && retab (convert tabs to spaces)
:auto BufWritePost  *.rb        %retab!
:auto BufWritePost  *.py        %retab!
:auto BufWritePost  *.htm       %retab!
:auto BufWritePost  *.html      %retab!
:auto BufWritePost  *.pp        %retab!
:auto BufWritePost  *.erb       %retab!
:auto BufWritePost  *.sh        %retab!
:auto BufWritePost  *.xml       %retab!

"                MAPPINGS               "
"""""""""""""""""""""""""""""""""""""""""
map <F2> :%retab <CR> 

" Use alt + arrows to something usefull
map <m-right> :bn<cr>
map <m-left>    :bp<cr>

" This keeps the current visual block selection active after changing indent with '<' or '>'. 
" Usually the visual block selection is lost after you shift it, which is incredibly annoying.
vmap > >gv
vmap < <gv
" Capital Y to yank the whole line (but without line break!)
nnoremap Y y$
" use spacebar for command mode 
nnoremap <Space> :

"        Command mode related           "
"""""""""""""""""""""""""""""""""""""""""
"Bash like keys for the command line
cnoremap <C-A> <Home>
cnoremap <C-E> <End>
cnoremap <C-K> <C-U>
cnoremap <C-P> <Up>
cnoremap <C-N> <Down>

"                PLUGINS                "
"""""""""""""""""""""""""""""""""""""""""
" ls -1 .vim/plugin:
""DrawItPlugin
""autoclose
""cecutil
""matchit
""minibufexpl
""utl
""utl_rc
""utl_scm
""utl_uri
" minibufexplorer 
let g:miniBufExplMapWindowNavArrows = 1 " control + arrow keys to move you between windows
let g:miniBufExplModSelTarget = 1
let g:miniBufExplorerMoreThanOne = 2
let g:miniBufExplModSelTarget = 0
let g:miniBufExplUseSingleClick = 1
let g:miniBufExplMapWindowNavVim = 1
"let g:miniBufExplVSplit = 15
let g:miniBufExplSplitBelow=0
let g:bufExplorerSortBy = "name"
autocmd BufRead,BufNew :call UMiniBufExplorer
map <leader>u :TMiniBufExplorer<cr>

" Enable filetype plugin + auto intentation
filetype plugin on
filetype indent on


"------------------------
"Delete trailing white space, useful for Python ;)
func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
"------------------------

"            FUNCTIONS                  "
"""""""""""""""""""""""""""""""""""""""""
function! ConvertTabsFrom2To4Expand()
    set tabstop=2
    set shiftwidth=2
    set noexpandtab
    retab!
    set tabstop=4
    set shiftwidth=4
    set expandtab
    retab!
endfunction

function! ConvertTabsFrom4To2Expand()
    set tabstop=4
    set shiftwidth=4
    set noexpandtab
    retab!
    set tabstop=2
    set shiftwidth=2
    set expandtab
    retab!
endfunction

""""""""""""  TIPS + TRICKS section """"""""""""
" INSERT MODE:
"   insert normal mode: <C-o>cmd
"     go to normal mode for one cmd
"     <C-o>zz
"   paste: <C-r>register
"   calculations: <C-r>=calc
"     <C-r>=6*33<Enter> :  198
" COMMAND MODE:
"   :[range]g/{pattern}/[cmd]
"     execute the ex cmd on all specified lines where pattern matches
" EX MODE:
"    repeate last ex-cmd: 
"      @:
"    execute a normal-mode cmd:
"      :[range]normal <cmd>
"    reveal a list of possible cmd-completions:
"      :cmd-stub<C-d>
"    Insert the contents of a numbered or named register
"      :<C-r>register
"    meet the command-line window:
"      open with history of ex commands:
"        q:
"      open with history of search commands:
"        q/
"      switch from cmd-line mode to cmd-line-window:
"        <C-f>
