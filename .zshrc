source $HOME/.zsh/checks.zsh
source $HOME/.zsh/setopt.zsh
source $HOME/.zsh/history.zsh
source $HOME/.zsh/functions.zsh
source $HOME/.zsh/path.zsh
source $HOME/.zsh/completions.zsh
source $HOME/.zsh/prompt.zsh
source $HOME/.zsh/aliases.zsh


if which -a tabs > /dev/null 2>&1; then
  # set $TERM tabsize to 2
  tabs -2
fi

(( ${+PAGER} ))  || export PAGER="less"
(( ${+EDITOR} )) || export EDITOR="vim"

case $(uname) in
  Linux)
    export EDITOR=vim
    export RUBYLIB=/home/schmidt/SOURCE/tmt/lib
    export MANPATH="$HOME/share/man:$MANPATH"
    export HIERADIR=$(find /etc/puppet -type d -name hieradata 2>/dev/null)
    PUPPET_MODULES_DIR="/etc/puppet/environments/production/modules"
    ;;

  Darwin)
    export EDITOR=$HOME/bin/subl
    export JAVA_HOME="/Library/Java/Home"
    export RUBYLIB=/Users/ts/SOURCE/tmt/lib
    export HOMEBREW_GITHUB_API_TOKEN='17eace8231a3b2836d98383c113005faa22a8d91'
    MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"   # manpath for gnu coreutils etc...
    ;;
esac

# shitty if forgotten so do it every login:
if [[ $USER = "schmidt" && $HOSTNAME == "tecpm01" || $HOSTNAME == "tecpm02" ]]; then
  cd $HOME/GIT/puppet/modules
  for i in $(/bin/ls) ; do
    echo "$i"
    cd $i
    git pull origin master
    cd ..
  done
  cd
fi

##------ SSH-AGENT --------
#if [[ $HOSTNAME == "puppet" ]]; then
#  SSH_AGENT=/usr/bin/ssh-agent
#  SSH_AGENT_ARGS="-s"
#  SSH_ADD="/usr/bin/ssh-add"
#  SSH_KEY="$HOME/.ssh/id_rsa"
#  SSH_AGENT_PIDFILE="$HOME/.agent.pid"
#
#  if [ -f "$SSH_AGENT_PIDFILE" ]; then
#    AGENT_PID=$(cat $SSH_AGENT_PIDFILE)
#    ps $AGENT_PID > /dev/null 2>&1
#    if [ $? != "0" ]; then
#      # Run it inside backticks, which will capture the output and
#      # pass it to 'eval' which will run it in your current shell.
#      eval `$SSH_AGENT $SSH_AGENT_ARGS`
#      eval echo $SSH_AGENT_PID > $SSH_AGENT_PIDFILE
#      if [ -f $HOME/.ssh/id_rsa ]; then
#        eval $SSH_ADD $SSH_KEY
#      fi
#    fi
#  fi
#
#  if [ ! -f "$SSH_AGENT_PIDFILE" ]; then
#    # Run it inside backticks, which will capture the output and
#    # pass it to 'eval' which will run it in your current shell.
#    eval `$SSH_AGENT $SSH_AGENT_ARGS`
#    eval echo $SSH_AGENT_PID > $SSH_AGENT_PIDFILE
#    if [ -f $HOME/.ssh/id_rsa ]; then
#      eval $SSH_ADD $SSH_KEY
#    fi
#  fi
#fi
##--------------------------

#compctl -K get_tmt_commands tmt
## Every GLI-powered app includes a built-in command called help that is mostly used for getting online help.
## This command also takes a switch and an optional argument you can use to facilitate tab completion.
## COMPREPLY=(`todo help -c $2`)
#function get_tmt_commands()
#{
  #if [ -z $2 ] ; then
    #COMPREPLY=(`tmt help -c`)
  #else
    #COMPREPLY=(`tmt help -c $2`)
  #fi
#}


# rescan iscsi bus (activate new vmdk without reboot)
## echo '- - -' > /sys/class/scsi_host/host<n>/scan
## change keyboard layout:
## loadkeys [de|..]

# very convenient shortcut for a common thing:
## > echo $(which zsh) < is equivalent with
## > echo =zsh

# erase all not x86_64 rpms:
## rpm -qa --queryformat='%{n}-%{v}-%{r}.%{arch}\n' | grep '\.i[3456]86$' | grep -v 'noarch' | xargs rpm -ev

#local _myhosts
#if [[ -f $HOME/.ssh/known_hosts ]]; then
#  _myhosts=( ${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[0-9]*}%%\ *}%%,*} )
#    zstyle ':completion:*' hosts $_myhosts
#    fi

#To reload a completion function:
## unfunction _func
## autoload -U _func
