setopt autocd extendedglob auto_pushd equals
setopt nullglob # workaround for rvm, see http://beginrescueend.com/integration/zsh/
setopt appendhistory share_history hist_ignore_all_dups hist_ignore_space
