export PS1=$'[%n@%m]:%~\n%? %# '

# (ohly) when doing a chdir command, update / get the actual ruby-version 
# from rbenv / rvm (to use i.e. in your prompt)
#function _update_ruby_version()
#{
#    typeset -g ruby_version=''
#    if which rvm-prompt &> /dev/null; then
#      ruby_version="$(rvm-prompt i v g)"
#      rvm-prompt i v g
#    else
#      if which rbenv &> /dev/null; then
#        ruby_version="$(rbenv version | sed -e "s/ (set.*$//")"
#      fi
#    fi
#}
#chpwd_functions+=(_update_ruby_version)
#
## prompt:
##export PS1=$'[%n@%m] $(_update_ruby_version):%~\n%? %# '
#export PS1=$'[%n@%m]:%~\n%? %# '