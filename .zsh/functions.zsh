# git log with pretty format 
function glog {
  git log --pretty=format:"(%an, %ad)   %s" --date=short 
}

# find the 10 newst files in a directory hierarchy
function newest {
  find $1 -type f -print0 | xargs -0 stat --format '%y %n' | sort -rn |head 
}
# open files that contain pattern with vim
function gvo {
  eval $EDITOR $(egrep -rl $@)
}

#print line of file
function pline {
  sed -n "$1p" $2
}

function noc {
  egrep -v '^\s*#|^"' $1 | sed '/^$/d'
}

function factgrep {
  facter | egrep $@
}

function hgrep {
  egrep -r $@ $HIERADIR/**/*.yaml
}
function hgrepv {
  vim $(egrep -rl $@ $HIERADIR/**/*.yaml)
}

function mine-default {
  mine $1 --rspec=false --mini-test=true --gem-package-task=true --bundler=true --bundler-tasks=true --rdoc=false --yard=true --markdown=true --gli-bin
}

function szh () {
  egrep "$@" $HOME/.zsh_history
}
function sbh () {
  egrep "$@" $HOME/.bash_history
}
function srh () {
  sudo egrep "$@" /root/.bash_history
}

function pmod () {
  for dir in $(/bin/ls -x1 /etc/puppet/environments/ts/modules/); do
    echo $dir
    PAGER="" cd /etc/puppet/environments/ts/modules/$dir && "$@"
  done
}

# 
function pushfag () {
  cd $PUPPET_MODULES_DIR
  for i in $(/bin/ls); do 
    if grep fag $i/.git/config > /dev/null ;then
      print $i
      cd $i
      git push fag master
      cd - > /dev/null
    fi
  done
}

function suc () {
  sudo su - $1 -c "$2"
}

function check-icinga-errors () {
  if [[ -d /usr/local/icinga ]]; then
    grep -i error /usr/local/icinga/var/icinga.chk
  fi
}

# progress feedback for long runners
progress(){
  echo -n "$0: Please wait..."
  while true
  do
    echo -n "."
    sleep 4
  done
}

# OS-DETECTION:
case $(uname -s) in
  Linux) os=linux ;;
  *[bB][sS][dD]) os=bsd ;;
  Darwin) os=mac ;;
  *) os=unix ;;
esac

case $os in
  linux)
    alias psa='ps axwo "user,pid,ppid,%cpu,cputime,%mem,rss,start_time,command"'
    ;;
  mac)
    alias psa='ps -A -o "start,user,pid,ppid,%cpu,%mem,rss,comm"'
    ;;
  esac
## using a function because:
## i need to insert the arguments passed to the is command within a command sequence
## so i can reference the arguments anywhere with the $@ variable
### psa ww <-- funzt nicht
function p () {
    psa | head -n1
    psa | grep -i "$@" | grep -v "grep"
}

#run-with-sudo, repeats last command with 'sudo' in front
run-with-sudo() { LBUFFER="sudo $LBUFFER" }
