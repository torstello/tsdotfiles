# helper function for setting PATH
pathmunge () {
 if ! echo $PATH | egrep -q "(^|:)$1($|:)" ; then
    if [ "$2" = "after" ] ; then
       PATH=$PATH:$1
    else
       PATH=$1:$PATH
    fi
 fi
}

pathmunge $HOME/bin
if [ -d $HOME/.rbenv ];then
  pathmunge 
  pathmunge "$HOME/.rbenv/bin"
fi
if [ -d $HOME/.linuxbrew/bin ];then
  pathmunge "$HOME/.linuxbrew/bin"
fi
pathmunge /usr/local/bin
pathmunge /usr/local/sbin

pathmunge /bin after
pathmunge /sbin after
pathmunge /usr/bin after
pathmunge /usr/sbin after
if [ -d $HOME/.cabal/bin ];then
  pathmunge "$HOME/.cabal/bin" after
fi
if [ -d /usr/local/share/npm/bin ];then
  pathmunge "/usr/local/share/npm/bin" after
fi
if [ -d /usr/local/heroku/bin ];then
  pathmunge "usr/local/heroku/bin" after
fi
# https://github.com/SublimeGit/SublimeGit/issues/150
if [ -d /usr/local/Cellar/git/2.1.2/libexec/git-core ];then
  pathmunge "/usr/local/Cellar/git/2.1.2/libexec/git-core" after
fi
if [ -d /usr/texbin ];then
  pathmunge "/usr/texbin" after
fi

if [ -d /usr/local/Cellar/rbenv/0.4.0/libexec ];then
  pathmunge "/usr/local/Cellar/rbenv/0.4.0/libexec" after
fi

export PATH="$PATH"

# on LINUX this puts "$HOME/.rbenv/shims" at the beginning of PATH:
if which rbenv >/dev/null 2>&1 ; then
  eval "$(rbenv init -)"
fi
