alias -g BQ='>/dev/null 2>&1 &'
alias -g C='| wc -l'
alias -g DEL="|sed '/^$/d'"
alias -g FLS='for i in $(/bin/ls) ; do '
alias -g FLS='for i in $(/bin/ls);do '
alias -g G='| egrep --color=auto'
alias -g H='| head'
alias -g K='| keep'
alias -g L='| less'
alias -g MP=' -a MPlayerX'
alias -g Nodoc=' --no-ri --no-rdoc'
alias -g P='| $PAGER'
alias -g S='| sort'
alias -g SL='| sort | less'
alias -g SUM="|awk '{sum +=\$1} END {print sum}'"
alias -g T='| tail'
alias -g V='| subl -n -'
alias ....='cd ../../..'
alias ...='cd ../..'
alias ..='cd ..'
alias 1='fg %1' 11='bg %1'
alias 2='fg %2' 22='bg %2'
alias 3='fg %3' 33='bg %3'
alias c='clear'
alias cu='sudo su -'
alias d='dirs -v'
alias egrep='egrep --color=auto'
alias gid="git diff"
alias gis='git status'
alias gpull="git pull origin master"
alias gpush="git push origin master"
alias grep='grep --color=auto'
alias j='jobs -l'
alias k='/bin/ls -altrh -F'
alias l='/bin/ls -alh -F'
alias ll='/bin/ls -lh -F'
alias llx='/bin/ls -lh -1 -F'
alias ls='/bin/ls -h -F'
alias lx='/bin/ls -x1 -F'
alias papplyts="sudo puppet apply $HOME/tspuppet/manifests/$USER.pp --modulepath=$HOME/tspuppet/modules"
alias reboot="sudo reboot"
alias sz="source $HOME/.zshrc"
alias hotfix-start="git-flow hotfix start "
alias hotfix-finish="git-flow hotfix finish -F -p -n "


if [ $IS_LINUX ]; then
  alias pmasterstaging="sudo puppet master --debug --no-daemonize \
                        --confdir=$HOME/GIT/puppet --ssldir=/etc/puppet/ssl \
                        --pidfile=/tmp/localpuppet_ts.pid --hiera_config=$HOME/GIT/puppet/hiera.yaml"
  alias pmasterprod="sudo puppet master --debug --no-daemonize \
                     --confdir=$HOME/GIT/puppet --ssldir=/etc/puppet/ssl \
                     --pidfile=/tmp/localpuppet_ts.pid --masterport=8140 "

  alias -g X='| xargs'
  alias -s log='vim'
  alias -s txt='vim'
  alias crep='createrepo --checksum sha .'
  alias tmp="/usr/bin/vim $HOME/tmp/tmp.txt"
  alias todo="eval $EDITOR $HOME/tmp/todo.txt"

  if eval which synclient >/dev/null 2>&1 ; then synclient touchpadoff=1 ; fi   # disable the annoying touchpad

  if eval grep ARCH /proc/version >/dev/null 2>&1 ; then
    alias halt="sudo systemctl poweroff"
    alias i='sudo pacman -S '
    alias orphans='sudo pacman -Qdt'
    alias qf='sudo pacman -Qo '
    alias ql='sudo pacman -Qi '
    alias r='sudo pacman -Rs '
    alias reboot="sudo systemctl reboot"
    alias s='sudo pacman -S --search'
    alias sy='sudo pacman -Sy'
    alias u='sudo pacman -Syu'
    alias wifi='sudo wifi-select'
    if eval which nitrogen >/dev/null 2>&1 ; then nitrogen --restore& ; fi  # workaround; it's not working in .config/openbox/autostart....

  elif [ $HAS_APT ]; then
    alias i="sudo apt-get install "
    alias qf='dpkg-query -S '
    alias ql='dpkg-query -L '
    alias r="sudo apt-get remove "
    alias s="apt-cache search "
    alias u="sudo apt-get update && sudo apt-get dist-upgrade"

  elif [ $HAS_YUM ]; then
    alias i="sudo yum install "
    alias list-gpg-keys="rpm -q gpg-pubkey --qf '%{name}-%{version}-%{release} --> %{summary}\n' "
    alias qf="rpm -qif "
    alias ql="rpm -qil "
    alias r="sudo yum erase "
    alias s="sudo yum search "
    alias u="sudo yum --disableexcludes=all update "
  fi
fi

if [ $IS_MAC ]; then
  alias tmp="subl $HOME/tmp/tmp.txt"
  alias updatedb='cd / && sudo /usr/local/bin/locate.updatedb'
  alias httpd='sudo launchctl load -w /System/Library/LaunchDaemons/org.apache.httpd.plist'
  alias s="brew search "
  alias sed='gsed'
  alias u="brew update && brew upgrade"
  alias i="brew install "
  alias r="brew remove "
  alias ql="brew list "
  alias netst='netstat -na -f inet  G -v udp'
  alias vlc='open -a VLC '
  alias airport=/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport
  alias -g X='| gxargs'
  alias -s txt='subl -n'
  alias -s log='subl -n'

  # homebrew syntax highlighting
  if [ -f /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ];then
    . /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  fi
fi
