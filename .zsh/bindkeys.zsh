# To see the key combo you want to use just do:
# cat > /dev/null
# And press it
bindkey "[A"      history-search-backward              # up arrow
bindkey "[B"      history-search-forward               # down arrow
bindkey "^A"      beginning-of-line                    # ctrl-a  
bindkey "^B"      backward-char                        # ctrl-b
bindkey "^D"      delete-char                          # ctrl-d
bindkey "^E"      end-of-line                          # ctrl-e
bindkey "^F"      forward-char                         # ctrl-f
bindkey "^K"      kill-whole-line                      # ctrl-k
bindkey "^R"      history-incremental-search-backward  # ctrl-r
bindkey "^[[1~"   beginning-of-line                    # Pos1
bindkey "^[[3~"   delete-char                          # del
bindkey "^[[4~"   end-of-line                          # END
bindkey '\ee'     edit-command-line                    # edit-command-line widget: esc -> e
bindkey '\es'     run-with-sudo                        # sudo widget: esc -> s
bindkey -e                                             # vi (-v) | emacs (-e) editing
