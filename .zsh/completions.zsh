zmodload -i zsh/complist

zstyle ':completion:*' group-name ''    # show categories, if possible
zstyle ':completion:*' menu select=3    # activate menu-compl. if choices > 2
zstyle ':completion:*:descriptions' format "%B%d%b"
zstyle ':completion:*:default' select-prompt '%SMatch %M   %P%s' # more compl. then space on terminal: display status-line
zstyle -e ':completion:*:approximate:*' max-errors 'reply-=($(( ($#PREFIX+$#SUFFIX)/3 )) numeric )' # allow 1 error per 3 letters

fpath=($fpath "$HOME/share/zsh/site-functions" "$HOME/.zsh/func")
typeset -U fpath  #??
#for i in $fpath ; do autoload $i ; done
autoload -U compinit && compinit

autoload edit-command-line # edit command line
autoload run-help          # escape+h | fast manpage access
autoload -Uz zcalc         # autoload zcalc calculator (to exit with ctrl+d)

zle -N edit-command-line
zle -N run-with-sudo